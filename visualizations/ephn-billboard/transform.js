/**
 * Transform Raw Data.  This matches the data schema you see in newrelic.com when choosing JSON.  However you can't use it to load charts directly.
 * In the NrqlQuery Component use: formatType={NrqlQuery.FORMAT_TYPE.RAW}
 */
export function transformRawData(rawData) {
    const keys = Object.keys(rawData.current.results[0])
    if (keys.length !== 1) {
        console.info('There are multiple keys detected in the query response.  Only using the first!');
        console.info('Keys: ' + keys.join(','));
    }

    const key = keys[0];
    const current = rawData.current.results[0][key];
    const previous = rawData.previous?.results[0][key];
    const percentage = getPercentage(current, previous);

    return {
        current,
        previous,
        percentage,
        key
    };
};

/**
 * Transform Chart Data.  This data schema is different than what you see in newrelic.com when choosing JSON.  However it can be passed into charts for direct loading.
 * In the NrqlQuery Component use: formatType={NrqlQuery.FORMAT_TYPE.CHART}
 */
export function transformChartData(chartData) {
    // Chart data has a number of keys that need to be filtered out to get the correct dynamic key
    const keys = Object.keys(chartData[0].data[0])

    // Filter out known keys
    const known_keys = ['begin_time', 'end_time', 'x', 'y'] // Chart Data
    const filtered_keys = keys.filter(key => known_keys.includes(key) === false)

    if (filtered_keys.length !== 1) {
        console.info('There are multiple keys detected in the query response.  Only using the first!');
        console.info('Keys: ' + keys.join(','));
    }

    const key = filtered_keys[0];

    // Format for return
    const current = chartData[0].data[0][key];
    const previous = chartData[1]?.data[0][key];
    const percentage = getPercentage(current, previous);

    return {
        current,
        previous,
        percentage,
        key
    };
}

function getPercentage(current, previous){
    // Try and calculate a reasonable threshold based on the metrics
    // This matches the built in billboard so the % shown is also the same as what is used here...
    if(!previous){
        return current * 100
    }

    return ((current - previous) / previous) * 100
}