# Changelog

## 0.6.0 (2022-05-26)
- Added rudimentary error boundary components
- Updated widget to handle new query builder by declaring default props (https://github.com/newrelic/nr1-status-widgets/pull/32)
- Added configuration information to modal (could use better styling...)

## 0.5.2 (2022-03-03)
- Updated GUID to allow publishing into master account
- Updated README with more information on publishing
- Updated percentage threshold calculation to work the same as the visualization when previous data is null or zero

## 0.5.1 (2022-02-25)
- Updated CSS to work in a 1x1 depoyed size

## 0.5.0 (2022-02-24)
- Added notes that can appear in a modal
- Added debugging logs for the widget values and platform state
- Updated configuration to use namespaces
- Updated healthy color to look better with light and dark themes
- Updated tooltip to appear when hovering bottom portion of widget
- Added health bar on the bottom for warning and error states

## 0.4.0 (2022-02-23)
- Updated text length with to be a fixed max with an overflow
- Added Tooltip for text
- Updated code structure by splitting out some functions into separate files

## 0.3.1 (2022-02-23)
- Fixed Percent Threshold

## 0.3.0 (2022-02-22)
- Added support for non COMPARE WITH clauses

## 0.2.0 (2022-02-18)
- Updated log levels

## 0.1.9 (2022-02-18)
- Updated to support more query types by using dynamic keys

## 0.1.5 (2022-02-18)
- Adding Flexbox

## 0.1.4 (2022-02-17)
- Adding documentation and screenshots

- - - - -
* Following [Semantic Versioning](https://semver.org/)
* [Changelog Formatting](https://ephesoft.atlassian.net/wiki/spaces/ZEUS1/pages/1189347481/Changelog+Formatting)
* [Major Version Migration Notes](MIGRATION.md)