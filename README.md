# nerdpack

## Getting started

Run the following scripts:

```
npm install
npm start
```

Visit https://one.newrelic.com/?nerdpacks=local and :sparkles:

## Creating new artifacts

If you want to create new artifacts run the following command:

```
nr1 create
```

> Create Visualization: `nr1 create --type visualization --name test-viz`.

## Serving up locally for testing
Serve up the local visualizations for viewing in New Relic.

```
npm run start
```

## Publishing to New Relic
Make sure you have configured nr1 to use the appropriate profile.  This is the account nr1 will publish your widget to.  You can see more details using commands like `npm run profiles:list` or view the documentation on [nr1 profiles](https://developer.newrelic.com/explore-docs/nr1-common#nr1-profiles).

If using a hierarchical account structure you will want to publish to your parent account so all children can access it.  

```
npm run publish
```

Once the component is published (have to wait for sync) you can publish the metadata catalog updates. [Documentation](https://developer.newrelic.com/build-apps/publish-deploy/catalog)

```
npm run publish:metadata
```

If you need to remove a published nerdpack from an account you can essentially hide it.  It does not appear to completely remove it, but it no longer shows up in the UI for selecting.

```
nr1 nerdpack:unsubscribe
nr1 nerdpack:undeploy
```

You can unpublish the nerdpack and then update the GUID's and republish to a new account.  Without changing the GUID it will attempt to publish to the existing account.  To see more details on where the nerdpack is published run:

```
nr1 nerdpack:info
```

## Future Development Work
These items were not completed during initial development and could be considered for future development.  To provide some context on any work on related tickets these features are logged here.

### Margin Issue
When shrinking the custom visualization to a 1x1 square you get a 16px margin that is applied above the iframe.  However, New Relic's custom visualization doesn't do this and has a special class applied.  See more details here for a question submitted to NR: https://discuss.newrelic.com/t/custom-visualization-margin-issue/177361

### Dark Mode Issue
When using Dark Mode in the browser you can't detect that it is being applied or take action easily.  Also, New Relics built in widgets somehow change their default success / warning... colors before being inverted.  You can follow a thread here for a question about dark mode and custom visualizations: https://discuss.newrelic.com/t/custom-nerdlet-get-theme-preferences/115038

### Page level Time Updates
When the default time is changed on a dashboard all the built in widgets automatically update to this time and override what was provided in the NRQL.  The custom widget can be updated to do the same using the PlatformStateContext (https://developer.newrelic.com/components/platform-state-context).  

Example Response Objects from PlatformStateContext
- Using default `{"tvMode":false,"accountId":2609460}`
- Using predefined time ranges `{"tvMode":false,"accountId":2609460,"timeRange":{"begin_time":null,"end_time":null,"duration":604800000}}`
- Using specific start / end times `{"tvMode":false,"accountId":2609460,"timeRange":{"begin_time":1644438304628,"end_time":1645043104628,"duration":null}}`

### Page level Filtering / Faceting
There doesn't appear to be a way in the platform as of 2022-02-25 to get this information.  A question was submitted to the communit related to this: https://discuss.newrelic.com/t/custom-visualization-platform-state-for-filters/177752