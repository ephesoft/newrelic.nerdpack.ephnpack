import React from 'react';
import { HeadingText, MultilineTextField, Spacing } from 'nr1'

export class Configuration extends React.Component {
    static defaultProps = {
        percent: {},
        count: {},
        nrql: ''
    };

    render() {
        return <div className="Configuration">
            <HeadingText type={HeadingText.TYPE.HEADING_1}>Configuration</HeadingText>
            <Spacing type={[Spacing.TYPE.OMIT, Spacing.TYPE.OMIT, Spacing.TYPE.LARGE, Spacing.TYPE.OMIT]}>
                <div>
                    <HeadingText type={HeadingText.TYPE.HEADING_4}>NRQL</HeadingText>
                    <MultilineTextField
                        readOnly
                        defaultValue={this.props.nrql}
                    />
                </div>
            </Spacing>
            <HeadingText type={HeadingText.TYPE.HEADING_4}>Percent Threshold</HeadingText>
            <Spacing type={[Spacing.TYPE.OMIT, Spacing.TYPE.OMIT, Spacing.TYPE.LARGE, Spacing.TYPE.LARGE]}>
                <div>
                    <HeadingText type={HeadingText.TYPE.HEADING_6}>Warning: {this.props.percent.warning ?? ''}</HeadingText>
                    <HeadingText type={HeadingText.TYPE.HEADING_6}>Error: {this.props.percent.error ?? ''}</HeadingText>
                </div>
            </Spacing>
            <HeadingText type={HeadingText.TYPE.HEADING_4}>Count Threshold</HeadingText>
            <Spacing type={[Spacing.TYPE.OMIT, Spacing.TYPE.OMIT, Spacing.TYPE.LARGE, Spacing.TYPE.LARGE]}>
                <div>
                    <HeadingText type={HeadingText.TYPE.HEADING_6}>Minimum Warning: {this.props.count.minWarning ?? ''}</HeadingText>
                    <HeadingText type={HeadingText.TYPE.HEADING_6}>Minimum Error: {this.props.count.minError ?? ''}</HeadingText>
                    <HeadingText type={HeadingText.TYPE.HEADING_6}>Maximum Warning: {this.props.count.maxWarning ?? ''}</HeadingText>
                    <HeadingText type={HeadingText.TYPE.HEADING_6}>Maximum Error: {this.props.count.maxError ?? ''}</HeadingText>
                </div>
            </Spacing>
        </div>
    }
};