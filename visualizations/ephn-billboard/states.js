import PropTypes from 'prop-types'
import { Card, CardBody, HeadingText } from 'nr1';

export function InvalidQueryState(props) {
    return <Card className="ErrorState">
        <CardBody className="ErrorState-cardBody">
            <HeadingText
                spacingType={[HeadingText.SPACING_TYPE.LARGE]}
                type={HeadingText.TYPE.HEADING_3}
            >
                Please provide a valid NRQL query.  If you are using a percent threshold you must provide a COMPARE WITH clause.
            </HeadingText>
            <HeadingText
                spacingType={[HeadingText.SPACING_TYPE.MEDIUM]}
                type={HeadingText.TYPE.HEADING_4}
            >
                An example NRQL query you can try is:
            </HeadingText>
            <code>
                SELECT count(*) from AwsLambdaInvocation SINCE 1 week ago COMPARE WITH 1 week ago
            </code>
        </CardBody>
    </Card>
};

export function EmptyState(props) {
    return <Card className="ErrorState">
        <CardBody className="ErrorState-cardBody">
            <HeadingText
                spacingType={[HeadingText.SPACING_TYPE.LARGE]}
                type={HeadingText.TYPE.HEADING_3}
            >
                Please provide an Account ID and Query.
            </HeadingText>
            <HeadingText
                spacingType={[HeadingText.SPACING_TYPE.MEDIUM]}
                type={HeadingText.TYPE.HEADING_4}
            >
                An example NRQL query you can try is:
            </HeadingText>
            <code>
                SELECT count(*) from AwsLambdaInvocation SINCE 1 week ago COMPARE WITH 1 week ago
            </code>
        </CardBody>
    </Card>
};

export function ErrorState(props) {
    return <Card className="ErrorState">
        <CardBody className="ErrorState-cardBody">
            <HeadingText
                className="ErrorState-headingText"
                spacingType={[HeadingText.SPACING_TYPE.LARGE]}
                type={HeadingText.TYPE.HEADING_3}
            >
                Oops! Something went wrong.
            </HeadingText>
            <HeadingText
                spacingType={[HeadingText.SPACING_TYPE.MEDIUM]}
                type={HeadingText.TYPE.HEADING_4}
            >
                Error: {props.message}
            </HeadingText>
        </CardBody>
    </Card>
};

ErrorState.propTypes = {
    message: PropTypes.string
}
