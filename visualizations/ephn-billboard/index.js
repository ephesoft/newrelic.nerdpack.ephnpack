import PropTypes from 'prop-types';
import React from 'react';
import { BillboardChart, Modal, NrqlQuery, PlatformStateContext, Spinner, Tooltip } from 'nr1';
import { Configuration } from './Configuration';
import { EmptyState, ErrorState, InvalidQueryState } from './states';
import { transformChartData } from './transform';

export default class TestVizVisualization extends React.Component {
    // Set default props.  New Query builder (when editing deployed widget) doesn't pass in the full structure
    // Only does a shallow merge, so not defining children properties
    static defaultProps = {
        query: {},
        percent: {},
        count: {},
        notes: [],
    };

    constructor() {
        super(...arguments);
        console.debug('Version: 0.6.0');

        this._onClick = this._onClick.bind(this);
        this._onClose = this._onClose.bind(this);
        this._onHideEnd = this._onHideEnd.bind(this);

        this.state = {
            hidden: true,
            mounted: false,
            status: ''
        };
    }

    static propTypes = {
        // NRQL Query
        query: PropTypes.shape({
            accountId: PropTypes.number,
            nrql: PropTypes.string,
        }),

        // Percentage Thresholds
        percent: PropTypes.shape({
            error: PropTypes.number,
            warning: PropTypes.number,
        }),

        // Count Thresholds
        count: PropTypes.shape({
            minError: PropTypes.number,
            minWarning: PropTypes.number,
            maxError: PropTypes.number,
            maxWarning: PropTypes.number,
        }),

        // Modal Notes
        notes: PropTypes.arrayOf(PropTypes.shape({
            note: PropTypes.string
        })),
    };

    _onClick() {
        this.setState({
            hidden: false,
            mounted: true,
        });
    }

    _onClose() {
        this.setState({ hidden: true });
    }

    _onHideEnd() {
        this.setState({ mounted: false });
    }

    _updateStatus = (status) => {
        if(this.state.status != status){
            this.setState({ status });
        }
    }

    validateQuery = (query) => {
        const _query = query.toLowerCase();
        const { error, warning } = this.props.percent;

        // Query must contain a compare with clause if using percentage validation
        if (error || warning) {
            if (_query.includes('compare with')) {
                return true;
            }
            return false;
        }
        return true;
    };

    getPercentageClasses = (data) => {
        const percentage = Math.abs(data.percentage);
        const { error, warning } = this.props.percent;

        // Check Error
        if (error && percentage >= error) {
            return ['error-percent'];
        }

        // Check Warning
        if (warning && percentage >= warning) {
            return ['warning-percent'];
        }

        return [];
    }

    getNumberClasses = (data) => {
        const { minError, maxError, minWarning, maxWarning } = this.props.count;

        // Check Error
        if ((minError && data.current <= minError) || (maxError && data.current >= maxError)) {
            return ['error-number']
        }

        // Check Warning
        if ((minWarning && data.current <= minWarning) || (maxWarning && data.current >= maxWarning)) {
            return ['warning-number']
        }

        return []
    }

    getStatusClasses = (data) => {
        // Get all classes into one array
        let classes = this.getNumberClasses(data);

        // Validate when doing a comparison
        if (data.percentage) {
            classes = classes.concat(this.getPercentageClasses(data));
        }

        // Return healthy if no issues found
        if(classes.length === 0 ){
            this._updateStatus('healthy');
            return ['healthy']
        }        

        // Return errors
        if(classes.find(item => item.match(/error/) )){
            this._updateStatus('error');
        }else {
            this._updateStatus('warning');
        }

        return classes
    };

    render() {
        const { accountId, nrql } = this.props.query;

        if (!accountId || !nrql) {
            return <EmptyState />;
        }

        if (!this.validateQuery(nrql)) {
            return <InvalidQueryState />;
        }

        // Create Modal
        let modal = '';
        if (this.state.mounted) {
            // Create Note List
            const notesList = this.props.notes.map(function (note) {
                return <p>{note.note}</p>;
            });

            modal = <Modal hidden={this.state.hidden} onClose={this._onClose} onHideEnd={this._onHideEnd}>
                <Configuration nrql={this.props.query?.nrql} percent={this.props.percent} count={this.props.count}/>
                <h1>Notes</h1>
                {notesList}
            </Modal>
        }

        return (
            <PlatformStateContext.Consumer>
                {(platformState) => {
                    return (
                        <>
                            {modal}
                            <NrqlQuery
                                query={nrql}
                                accountIds={[parseInt(accountId)]}
                                pollInterval={NrqlQuery.AUTO_POLL_INTERVAL}
                                formatType={NrqlQuery.FORMAT_TYPE.CHART}
                            >
                                {({ data, loading, error }) => {
                                    if (loading) {
                                        return <Spinner />;
                                    }

                                    if (error) {
                                        console.log(error);
                                        return <ErrorState message={error.message} />;
                                    }

                                    const transformedData = transformChartData(data);

                                    return (
                                        <div className='ephn-wrapper'>
                                            <div className='ephn-clickable' onClick={this._onClick}>
                                            < BillboardChart className={`ephn-billboard ${this.getStatusClasses(transformedData).join(' ')}`}
                                                accountIds={[parseInt(accountId)]}
                                                data={data}
                                            />
                                            </div>
                                            <Tooltip text={transformedData.key} placementType={Tooltip.PLACEMENT_TYPE.TOP}>
                                                <div className='ephn-tooltip-bar'></div>
                                            </Tooltip>
                                            <div className={`ephn-health-bar ${this.state.status}`}></div>
                                        </div>
                                    );
                                }}
                            </NrqlQuery>
                        </>
                    )
                }}
            </PlatformStateContext.Consumer>
        );
    }
}
