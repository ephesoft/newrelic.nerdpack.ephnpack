// TODO: Should split out into multiple components so I can create an error boundary at the top level to catch and display any errors in subcomponents...
export class ErrorBoundary extends React.Component {
    constructor(props) {
      super(props);
      this.state = { 
          hasError: false,
          error: {},
          errorInfo: {}
        };
    }
  
    static getDerivedStateFromError(error) {
      // Update state so the next render will show the error UI.
      return { hasError: true };
    }
  
    componentDidCatch(error, errorInfo) {
        this.setState({
            error,
            errorInfo,
        });
    }
  
    render() {
        if (this.state.hasError) {
            // You can render any custom fallback UI
            return <div>
                    <h1>Error:</h1>
                    <p>{JSON.stringify(this.state.error)}</p>
                    <h2>Error Info:</h2>
                    <p>{JSON.stringify(this.state.errorInfo)}</p>
                </div>;
        }

        return this.props.children;
    }
  }