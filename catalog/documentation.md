## Ephn Pack

This nerdpack contains items built by the Semantik team for Ephesoft.

- ephn-billboard widget

## ephn-billboard Features

- Warning and Error Percent Threshold
- Warning and Error Max / Min Threshold

---